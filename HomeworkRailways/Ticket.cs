﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkRailways
{
    public class Ticket : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime TravelDate { get; set; } = DateTime.Now;
        public string IdentityCard { get; set; }

        public string TrainNumber { get; set; }
        public int CarriageNumber { get; set; }
        public int CoupeNumber { get; set; } // TODO сделать if На купе
        public int SeatNumber { get; set; }

        public bool Payed { get; set; } = false;
    }
}
