namespace HomeworkRailways.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deletedbset : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Carriages");
            DropTable("dbo.Trains");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Trains",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TrainNumber = c.String(),
                        TrainDirection = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Carriages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
