﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkRailways
{
    public class Carriage : Entity
    {
        // ненужный класс оказался, ничего из этого не используется, не удаляю, потому что писала несколько дней
        // с перерывами на работе и вдруг где-то что-то да упадет
        public int[] CarriageCoupe { get; set; } = { 1, 2, 3, 4, 5, 6, 7, 8, 9 }; // в вагоне девять купе по 4 места. так сказал гугл
    }
}
