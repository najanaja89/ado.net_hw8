﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using HomeworkRailways.ShowCoupe;


namespace HomeworkRailways
{
    public class TicketOrder : Entity
    {
        public void Order(int chosenCarriage, int chosenSeat)
        {
            using (var context = new TicketContext())
            {
                Ticket ticket = new Ticket();
                Console.WriteLine("First name");
                ticket.FirstName = Console.ReadLine();
                Console.WriteLine("Last name");
                ticket.LastName = Console.ReadLine();
                Console.WriteLine("Identity card");
                ticket.IdentityCard = Console.ReadLine();

                #region Payment
                Console.WriteLine("Invoice for payment: ");
                Random random = new Random();
                Console.WriteLine(random.Next(500, 1000) + "t. (Y/N)");
                var answer = Console.ReadLine().ToLower();
                if (answer == "y")
                {
                    Console.WriteLine("Payment received.\nTicket purchased");
                    ticket.Payed = true;
                }
                else if (answer == "n")
                {
                    Console.WriteLine("Seat is chosen.\nWaiting for payment");
                    ticket.Payed = false;
                }
                else
                {
                    Console.WriteLine("Error");
                }
                #endregion

                ticket.TrainNumber = "ABC123";
                if (chosenCarriage > 0 && chosenCarriage < 11) // 10 вагонов от 1 до 10
                {
                    ticket.CarriageNumber = chosenCarriage;
                }
                else
                {
                    Console.WriteLine("Wrong carriage");
                }
                ticket.SeatNumber = chosenSeat;
                //долгий if на выбор купе, ничего элегантнее не придумалось
                #region Coupe

                if (chosenSeat > 0 && chosenSeat < 5)
                {
                    ticket.CoupeNumber = 1;
                }
                if (chosenSeat > 4 && chosenSeat < 9)
                {
                    ticket.CoupeNumber = 2;
                }
                if (chosenSeat > 8 && chosenSeat < 13)
                {
                    ticket.CoupeNumber = 3;
                }
                if (chosenSeat > 12 && chosenSeat < 17)
                {
                    ticket.CoupeNumber = 4;
                }
                if (chosenSeat > 16 && chosenSeat < 21)
                {
                    ticket.CoupeNumber = 5;
                }
                if (chosenSeat > 20 && chosenSeat < 25)
                {
                    ticket.CoupeNumber = 6;
                }
                if (chosenSeat > 24 && chosenSeat < 29)
                {
                    ticket.CoupeNumber = 7;
                }
                if (chosenSeat > 28 && chosenSeat < 33)
                {
                    ticket.CoupeNumber = 8;
                }
                if (chosenSeat > 32 && chosenSeat < 37)
                {
                    ticket.CoupeNumber = 9;
                }
                #endregion
                context.Tickets.Add(ticket);
                context.SaveChanges();
            }
        }
    }
}
