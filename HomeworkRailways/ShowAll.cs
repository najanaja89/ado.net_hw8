﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkRailways
{
    public class ShowAll
    {
        TicketOrder ticketOrder = new TicketOrder();
        List<int> seat = new List<int>();

        public void ShowCoupe()
        {
            Console.WriteLine("| {0}  {1} | {2}  {3} | {4}  {5} |  {6}  {7} |  {8}  {9} |  {10}  {11} |  {12}  {13} |  {14}  {15} |  {16}  {17} |",
                "2", "4", "6", "8", "10", "12", "14", "16", "18", "20", "22", "24", "26", "28", "30", "32", "34", "36");
            Console.WriteLine("| {0}  {1} | {2}  {3} | {4}   {5} |  {6}  {7} |  {8}  {9} |  {10}  {11} |  {12}  {13} |  {14}  {15} |  {16}  {17} |",
                "1", "3", "5", "7", "9", "11", "13", "15", "17", "19", "21", "23", "25", "27", "29", "31", "33", "35");

            int seatCount = 36;
            // рандом для определения свободных мест 
            Random random = new Random();
            int freeSeat = random.Next(0, seatCount);
            int key;
            for (int i = 0; i < freeSeat; i++)
            {
                key = random.Next(1, seatCount);
                seat.Add(key);
            }
            seat = seat.Distinct().ToList(); // удаление повторяющихся значений
            Console.WriteLine("\nTotal number of free seats = " + seat.Count);
            seat.Sort();
            Console.WriteLine("Free seats: ");
            foreach (int i in seat)
            {
                Console.Write(i + " ");
            }
        }

        public void Show(int chosenCarriage, int chosenSeat)
        {
            if (seat.Contains(chosenSeat))
            {
                ticketOrder.Order(chosenCarriage, chosenSeat);
            }
            else Console.WriteLine("Error");
        }

        public void ShowTicket()
        {
            using (var context = new TicketContext())
            {
                var ticketList = context.Tickets.ToList();
                Console.WriteLine("Choose name");
                Console.WriteLine("{0}. {1, 0} \t{2, 10}", "N", "First name", "Last name");
                for (int i = 0; i < ticketList.Count; i++)
                {
                    Console.WriteLine("{0}. {1, 0} \t{2, 8}", i + 1, ticketList.ElementAt(i).FirstName, ticketList.ElementAt(i).LastName);
                }
                int nameNumber = int.Parse(Console.ReadLine());
                Console.WriteLine("{0}. {1, 0} \t{2, 8} \t{3, 8} \t\t{4, 8} \t{5, 8} {6, 5}  {7, 5} \t{8, 5} {9, 9}", "N", "First name", "Last name",
                    "Travel date", "Identity card", "Train number", "Carriage", "Coupe", "Seat", "Payed");
                if (nameNumber <= ticketList.Count)
                {
                    Console.WriteLine("{0}. {1, 0} \t{2, 8} \t{3, 8} \t{4, 8} \t{5, 8} {6, 8} {7, 8} {8, 8} \t{9, 7}", 1, 
                        ticketList.ElementAt(nameNumber - 1).FirstName, ticketList.ElementAt(nameNumber - 1).LastName,
                        ticketList.ElementAt(nameNumber - 1).TravelDate, ticketList.ElementAt(nameNumber - 1).IdentityCard, ticketList.ElementAt(nameNumber - 1).TrainNumber,
                        ticketList.ElementAt(nameNumber - 1).CarriageNumber, ticketList.ElementAt(nameNumber - 1).CoupeNumber, ticketList.ElementAt(nameNumber - 1).SeatNumber, ticketList.ElementAt(nameNumber - 1).Payed);
                }
                else
                {
                    Console.WriteLine("error");
                }
            }
        }
    }
}
